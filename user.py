import json

class User(object):
    def __init__(self, user_id, name, surname, date):
        self.id = user_id
        self.name = name
        self.surname = surname
        self.date = date

    def get_json(self):
        return json.dumps({'id': self.id, 'name': self.name, 'surname': self.surname, 'birthDate': self.date})

    @staticmethod
    def set_from_json(json_object):
        user_dict = json.loads(json_object)
        return User(
            user_id=user_dict['id'],
            name=user_dict['name'],
            surname=user_dict['surname'],
            date=user_dict['birthDate']
        )
