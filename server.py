import json

from flask import Flask, jsonify, request

from user import User

app = Flask(__name__)

user_list = [
    User(user_id=1, name="A", surname="G", date="1995-06-22"),
    User(user_id=2, name="B", surname="H", date="1994-05-23"),
    User(user_id=3, name="C", surname="S", date="1993-04-24"),
    User(user_id=4, name="D", surname="R", date="1992-03-25"),
    User(user_id=5, name="E", surname="Q", date="1991-02-26"),
]

mapping = {
    'name':'name',
    'surname':'surname',
    'birthDate':'date',
    'id': 'id'
}



def get_user_from_list(user_id):
    user = filter(lambda user: user.id == int(user_id), user_list)
    if user:
        return user[0]


@app.route('/user/<user_id>', methods=['GET'])
def get_user(user_id):
    user = get_user_from_list(user_id=user_id)
    if user:
        return user.get_json()


@app.route('/user/', methods=['POST'])
def add_user():
    user = User.set_from_json(request.data)
    user_list.append(user)
    return jsonify({'result': 'OK'})


@app.route('/user/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    user = get_user_from_list(user_id=user_id)
    if not user:
        return jsonify({'result': 'NOT OK'})
    user_list.remove(user)
    return jsonify({'result': 'OK'})


@app.route('/user/', methods=['PUT'])
def edit_user():
    data = json.loads(request.data)
    user_id = data['id']
    user = get_user_from_list(user_id=user_id)
    user_list.remove(user)
    user = User(None, None, None, None)
    for key in data:
        user.__setattr__(mapping[key], data[key])
    user_list.append(user)
    return jsonify({'result': 'OK'})


@app.route('/user/', methods=['PATCH'])
def update_user():
    data = json.loads(request.data)
    user_id = data['id']
    user = get_user_from_list(user_id=user_id)
    user_list.remove(user)
    for key in data:
        user.__setattr__(mapping[key], data[key])
    user_list.append(user)
    return jsonify({'result': 'OK'})


if __name__ == '__main__':
    app.run()

